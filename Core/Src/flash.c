/*
 * flash.c
 *
 *  Created on: Jan 18, 2021
 *      Author: timmy
 */
#include "flash.h"
#include "spi.h"

#define FLASH_RESET_COUNT	20

#define flash_assert(cond, error)	if(!cond){return flash_abort(error, response);}

static uint16_t flash_abort(flash_error_e error, uint8_t* response);

static uint16_t flash_compareVersion(uint32_t version, uint8_t* response);
static uint16_t flash_writeData(uint32_t address, uint16_t length, uint8_t* data, uint8_t* response);
static uint16_t flash_bootApplication(uint32_t version, uint8_t* data, uint8_t* response);

static void flash_jumpToApplication(void);
static uint32_t flash_calcCrc(uint32_t length);

static volatile const flash_config_t flash_config __attribute__ ((section (".configSection"))) = {
	.length = 0x00000000,
	.crc = 0x00000000,
	.version = 0x0000000
};
static CRC_HandleTypeDef* flash_crc;
static TIM_HandleTypeDef* flash_tim;

static uint32_t flash_resetCounter __attribute__ ((section (".countSection")));

/**
  * @brief  Initializes the flash handler with the CRC handle
  * @param[in]  thisCrc pointer to a CRC_HandleTypeDef structure that contains
  *         the configuration information for CRC module.
  * @retval None
  */
void flash_init(CRC_HandleTypeDef* thisCrc, TIM_HandleTypeDef* thisTim)
{
	flash_crc = thisCrc;
	flash_tim = thisTim;
}

/**
  * @brief  Checks whether or not a valid application is loaded and boots
  * 		accordingly.
  * @retval None
  */
void flash_checkBootState(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOA_CLK_ENABLE();

	/*Configure GPIO pin : SPI_CS_Pin */
	GPIO_InitStruct.Pin = SPI_CS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SPI_CS_GPIO_Port, &GPIO_InitStruct);

	if(!__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST))
	{
		if(flash_config.length > 0)
		{
			if(flash_config.crc == flash_calcCrc(flash_config.length))
			{
				flash_jumpToApplication();
			}
		}
	}

	__HAL_RCC_CLEAR_RESET_FLAGS();
}

/**
  * @brief  Checks whether the switch is trying to reset the LoRa module
  * 		to re-activate the bootloader. If so, performs a software reset.
  * @retval None
  */
void flash_checkReset(void)
{
	WRITE_REG(IWDG->KR, IWDG_KEY_RELOAD);
	if(HAL_GPIO_ReadPin(SPI_CS_GPIO_Port, SPI_CS_Pin) == GPIO_PIN_RESET)
	{
		if(flash_resetCounter++ > FLASH_RESET_COUNT)
		{
			HAL_NVIC_SystemReset();
		}
	}
	else
	{
		flash_resetCounter = 0;
	}
}

/**
  * @brief  Processes the message based on the header.type
  * @param[in]  header pointer to spi_header_t that contains the
  * 		header data to be processed.
  * @param[out] response pointer to uint8_t data that will contain
  * 		the response to the incoming message.
  *
  * @retval responseLength uint16_t length of the response to the message.
  */
uint16_t flash_handleMessage(spi_header_t* header, uint8_t* response)
{
	uint16_t responseLength;

	/*handle based on the type of message*/
	switch(header->type)
	{
		case 0x00:
		{
			responseLength = flash_compareVersion(header->address, response);
			break;
		}

		case 0x01:
		{
			responseLength = flash_writeData(header->address, header->length, header->data, response);
			break;
		}

		case 0x02:
		{
			responseLength = flash_bootApplication(header->address, header->data, response);
			break;
		}

		default:
		{
			/*Invalid type so force a negative assertion.*/
			flash_assert((0), flashError_invalidType);
			break;
		}

	}

	return responseLength;
}

/**
  * @brief  Called when an assertion has been made as a result of
  * 		an error.
  * @param[in]  error flash_error_e the error that was asserted.
  * @param[out] response pointer to uint8_t data that will contain
  * 		the response to the incoming message.
  *
  * @retval responseLength uint16_t length of the response to the message.
  */
static uint16_t flash_abort(flash_error_e error, uint8_t* response)
{
	response[0] = (uint8_t)error;
	response[1] = 0x00;
	response[2] = 0x00;

	return SPI_MIN_RESPONSE_SIZE;
}

/**
  * @brief  Compares the current version loaded and the requested update.
  * @param[in]  version uint32_t hexadecimal version.
  * @param[out] response pointer to uint8_t data that will contain
  * 		the response to the incoming message.
  *
  * @retval responseLength uint16_t length of the response to the message.
  */
static uint16_t flash_compareVersion(uint32_t version, uint8_t* response)
{
	/*check the update is newer than the current version*/
	flash_assert((version > flash_config.version), flashError_versionOlder);

	response[0] = (uint8_t)flashError_noError;
	response[1] = 0x00;
	response[2] = 0x00;

	return SPI_MIN_RESPONSE_SIZE;
}

/**
  * @brief  Writes data to flash of specified length at a specified location
  * 		in Memory.
  * @param[in]  address uint32_t address to which data is to be written.
  * @param[in]  length uint16_t length of the data in bytes to be written.
  * @param[in]  data pointer to uint8_t data that is to be written.
  * @param[out] response pointer to uint8_t data that will contain
  * 		the response to the incoming message.
  *
  * @retval responseLength uint16_t length of the response to the message.
  */
static uint16_t flash_writeData(uint32_t address, uint16_t length, uint8_t* data, uint8_t* response)
{
	HAL_StatusTypeDef status;
	FLASH_EraseInitTypeDef eraseType;
	uint32_t pageError, startAddress, endAddress, dataWord;
	extern uint8_t __app_start__;
	extern uint8_t __app_end__;
	extern uint8_t TIM21_IRQHandler;

	endAddress = (uint32_t)&__app_end__;
	startAddress = (uint32_t)&__app_start__;

	/*Check that the address requested is in the valid application range.*/
	flash_assert((address >= startAddress), flashError_addressRange);
	flash_assert(((address + length) <= endAddress), flashError_addressRange);

	/*Check that there is data to be written*/
	flash_assert((length > 0), flashError_invalidLength);

	/*check that the address is in line with a page.*/
	flash_assert(((address % FLASH_PAGE_SIZE) == 0), flashError_addressOffset);

	/*Unlock the flash to enable erase and writes.*/
	status = HAL_FLASH_Unlock();
	flash_assert((status == HAL_OK), flashError_lockingFailed);

	/*Loop through the data a page at a time.*/
	for(uint16_t i = 0; i < length; i += FLASH_PAGE_SIZE)
	{
		/*erase the current page.*/
		eraseType.TypeErase = FLASH_TYPEERASE_PAGES;
		eraseType.NbPages = 1;
		eraseType.PageAddress = address + i;
		status = HAL_FLASHEx_Erase(&eraseType, &pageError);
		flash_assert((status == HAL_OK), flashError_eraseFailed);

		/*write the data 1 word at a time.*/
		for(uint16_t j = 0; j < FLASH_PAGE_SIZE; j += sizeof(uint32_t))
		{
			/*check that we do not overrun the amount of data to be written*/
			if(i + j >= length)
			{
				break;
			}
			dataWord = data[i + j + 3] << 24;
			dataWord |= data[i + j + 2] << 16;
			dataWord |= data[i + j + 1] << 8;
			dataWord |= data[i + j];

			if((address + i + j) == (startAddress + sizeof(uint32_t)*(TIM21_IRQn + 16)))
			{
				dataWord = (uint32_t)&TIM21_IRQHandler;
			}

			status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address + i + j, dataWord);
			flash_assert((status == HAL_OK), flashError_writeFailed);
		}
	}

	/*Lock the flash to disable erase and writes.*/
	status = HAL_FLASH_Lock();
	flash_assert((status == HAL_OK), flashError_lockingFailed);

	response[0] = (uint8_t)flashError_noError;
	response[1] = 0x00;
	response[2] = 0x00;

	return SPI_MIN_RESPONSE_SIZE;
}

/**
  * @brief  Boots the application if the HEX has been flashed correctly. Updates
  * 		the config if the HEX loaded is valid
  * @note	a known CRC of the HEX file is sent for comparison with a CRC calculated
  * 		across the application memory space.
  * @param[in]  version uint32_t hexadecimal version.
  * @param[in]  data pointer to uint8_t data that contains the HEX CRC and No. of Blocks.
  * @param[out] response pointer to uint8_t data that will contain
  * 		the response to the incoming message.
  *
  * @retval responseLength uint16_t length of the response to the message.
  */
static uint16_t flash_bootApplication(uint32_t version, uint8_t* data, uint8_t* response)
{
	HAL_StatusTypeDef status;
	FLASH_EraseInitTypeDef eraseType;
	uint32_t pageError, crcCalc, address, crc, length;
	flash_config_t newConfig;
	extern uint8_t __config_start__;

	length = data[0] << 24;
	length |= data[1] << 16;
	length |= data[2] << 8;
	length |= data[3];

	crc = data[4] << 24;
	crc |= data[5] << 16;
	crc |= data[6] << 8;
	crc |= data[7];

	crcCalc = flash_calcCrc(length);
	flash_assert((crcCalc == crc || crc == crcCalc - 1), flashError_crcMismatch);

	status = HAL_FLASH_Unlock();
	flash_assert((status == HAL_OK), flashError_lockingFailed);

	/*erase the config page.*/
	address = (uint32_t)&__config_start__;

	eraseType.TypeErase = FLASH_TYPEERASE_PAGES;
	eraseType.NbPages = 1;
	eraseType.PageAddress = address;
	status = HAL_FLASHEx_Erase(&eraseType, &pageError);
	flash_assert((status == HAL_OK), flashError_eraseFailed);

	/*update the config info*/
	newConfig.crc = crcCalc;
	newConfig.version = version;
	newConfig.length = length;

	for(uint16_t j = 0; j < sizeof(flash_config_t); j += sizeof(uint32_t))
	{
		status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address + j, *(uint32_t*)(((uint8_t*)&newConfig) + j));
		flash_assert((status == HAL_OK), flashError_writeFailed);
	}

	/*Lock the flash to disable erase and writes.*/
	status = HAL_FLASH_Lock();
	flash_assert((status == HAL_OK), flashError_lockingFailed);

	__HAL_RCC_CLEAR_RESET_FLAGS();

	response[0] = (uint8_t)flashError_noError;
	response[1] = 0x00;
	response[2] = 0x00;

	return SPI_MIN_RESPONSE_SIZE;
}

/**
  * @brief  Calculates the CRC of the application memory space given
  * 		a number of pages.
  * @param[in]  length uint32_t no of pages across which the calculation
  * 		is to be made.
  * @retval crc uint32_t calculate crc.
  */
static uint32_t flash_calcCrc(uint32_t length)
{
	extern uint8_t __app_start__;

	uint32_t* appStart = (uint32_t*)&__app_start__;
	uint32_t size = length*FLASH_PAGE_SIZE;

	return HAL_CRC_Calculate(flash_crc, appStart, size) ^ 0xFFFFFFFF;
}

/**
  * @brief  De-initializes all peripherals and clocks before jumping to
  * 		application Memory.
  * @retval None.
  */
static void flash_jumpToApplication(void)
{
    void (*SysMemBootJump)(void);
    extern uint8_t __app_start__;
    volatile uint32_t addr = (uint32_t)&__app_start__;

    /*Reset all Hardware configs.*/
    HAL_DeInit();

    /*Disable RCC, set it to default (after reset) settings*/
    HAL_RCC_DeInit();

    flash_resetCounter = 0;
    HAL_TIM_Base_Start_IT(flash_tim);

    /*Disable systick timer and reset it to default values*/
	SysTick->CTRL = 0;
	SysTick->LOAD = 0;
	SysTick->VAL = 0;

    /*Set jump memory location for system memory*/
    SysMemBootJump = (void (*)(void)) (*((uint32_t *)(addr + 4)));

    /*Set main stack pointer*/
    __set_MSP(*(uint32_t *)addr);

    /*Actually call our function to jump to set location*/
    SysMemBootJump();
}
