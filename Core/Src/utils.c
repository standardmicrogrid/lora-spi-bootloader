/*
 * utils.c
 *
 *  Created on: 18 Jan 2021
 *      Author: timmy
 */
#include "utils.h"

static uint32_t flags = 0, flagsWaiting = 0;

/**
  * @brief  Sets a flag to be registered globally.
  * @param[in]  thisFlag utils_flag_e flag being set
  * @retval None
  */
void utils_setFlag(utils_flag_e thisFlag)
{
	uint32_t flagBit = (uint32_t)(1 << (uint8_t)thisFlag);

	if(flagsWaiting & flagBit)
	{
		flags |= flagBit;
		flagsWaiting &= ~flagBit;
	}
}

/**
  * @brief  Clears a flag that has been set.
  * @param[in]  thisFlag utils_flag_e flag being cleared
  * @retval None
  */
void utils_clearFlag(utils_flag_e thisFlag)
{
	flags &= ~(uint32_t)(1 << (uint8_t)thisFlag);
}

/**
  * @brief  checks whether a flag was set.
  * @param[in]  thisFlag utils_flag_e the flag to be checked.
  * @retval valid bool
  */
bool utils_checkFlag(utils_flag_e thisFlag)
{
	return ((uint32_t)(1 << (uint8_t)thisFlag) & flags);
}

/**
  * @brief  Waits until a specified flag has been set or a timeout
  * 		is reached.
  * @param[in]  msTimoeut	uint32_t msTimeout period in which a flag must
  * 		be set or waiting is abandoned.
  * @param[in]  num	int number of flags to be waited for
  * @param[in]  ...	utils_flag_e List of flags to be waited for.
  * @retval None
  */
void utils_awaitFlags(uint32_t msTimeout, int num, ...)
{
	va_list marker;
	uint32_t flagBits = 0;
	uint32_t tickstart;
	va_start(marker, num);

	for(int i = 0; i < num; i++)
	{
		flagBits |= (uint32_t)(1 << (uint8_t)va_arg(marker, int));

	}

	va_end(marker);

	flagsWaiting |= flagBits;
	flags &= ~flagBits;

	tickstart = HAL_GetTick();
	while(!(flags & flagBits) && ((HAL_GetTick() - tickstart) <= msTimeout));

	flagsWaiting &= ~flagBits;

}
