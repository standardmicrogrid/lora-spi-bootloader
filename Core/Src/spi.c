/*
 * spi.c
 *
 *  Created on: Jan 18, 2021
 *      Author: timmy
 */
#include "main.h"
#include "spi.h"
#include "utils.h"
#include "flash.h"

#define SPI_HEADER_SIZE		7
#define SPI_MAX_DATA_SIZE	1024
#define SPI_CRC_SIZE		sizeof(uint32_t)
#define SPI_BUFFER_SIZE		SPI_HEADER_SIZE + SPI_MAX_DATA_SIZE + SPI_CRC_SIZE

#define spi_assert(cond, error)	if(!cond){return spi_abort(error, response);}

static SPI_HandleTypeDef* spi_spi;
static CRC_HandleTypeDef* spi_crc;
static IWDG_HandleTypeDef* spi_iwdg;
static uint8_t spi_txBuffer[SPI_BUFFER_SIZE];
static uint8_t spi_rxBuffer[SPI_BUFFER_SIZE];

static uint16_t spi_abort(spi_error_e error, uint8_t* response);
static void spi_prepareToTxRx(uint16_t size, uint8_t* txBuffer, uint8_t* rxBuffer);
static void spi_handleIncomingHeader(void);
static uint16_t spi_handleIncomingData(spi_header_t* header, uint8_t* response);
static uint32_t spi_calcCrc(uint8_t* data, uint16_t length);
static void spi_notifyBusy(void);
static void spi_notifyReady(void);

/**
  * @brief  Initialise the SPI handler with the SPI handle
  * @param[in]  thisHandle pointer to a SPI_HandleTypeDef structure that contains
  *         the configuration information for SPI module.
  * @retval None
  */
void spi_init(SPI_HandleTypeDef* thisSpi, CRC_HandleTypeDef* thisCrc, IWDG_HandleTypeDef* thisIwdg)
{
	spi_spi = thisSpi;
	spi_crc = thisCrc;
	spi_iwdg = thisIwdg;
}

/**
  * @brief  Poll for incoming messages received over SPI.
  * @retval None
  */
void spi_checkIncoming(void)
{
	//Wait for a falling edge or a timeout
	utils_awaitFlags(10000, 1, flagSpiCsFalling);
	//if a CS falling edge was detected.
	if(utils_checkFlag(flagSpiCsFalling))
	{
		//prepare to receive the message.
		spi_prepareToTxRx(SPI_HEADER_SIZE, spi_txBuffer, spi_rxBuffer);
		//Wait for the SPI transaction to complete.
		utils_awaitFlags(50, 1, flagSpiTxRxDone);
		//If it completed successfully
		if(utils_checkFlag(flagSpiTxRxDone))
		{
			//handle the message
			spi_handleIncomingHeader();
		}
	}
}

/**
  * @brief  Callback that occurs when an SPI TX/RX transaction has been completed.
  * @param[in]  thisHandle pointer to a SPI_HandleTypeDef structure that contains
  *         the configuration information for SPI module.
  * @retval None
  */
void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef* thisHandle)
{
	if(thisHandle == spi_spi)
	{
		utils_setFlag(flagSpiTxRxDone);
	}
}

/**
  * @brief  Called when an assertion has been made as a result of
  * 		an error.
  * @param[in]  error spi_error_e the error that was asserted.
  * @param[out] response pointer to uint8_t data that will contain
  * 		the response to the incoming message.
  *
  * @retval responseLength uint16_t length of the response to the message.
  */
static uint16_t spi_abort(spi_error_e error, uint8_t* response)
{
	response[0] = (uint8_t)error;
	response[1] = 0x00;
	response[2] = 0x00;

	return SPI_MIN_RESPONSE_SIZE;
}

/**
  * @brief  Prepare to send data over SPI as a SLAVE.
  * @param[in]  size the number of bytes to be transferred.
  *
  * @retval None
  */
static void spi_prepareToTxRx(uint16_t size, uint8_t* txBuffer, uint8_t* rxBuffer)
{
	HAL_SPI_TransmitReceive_DMA(spi_spi, txBuffer, rxBuffer, size);
}

/**
  * @brief  Notify the master that a message is being processed and
  * 		that it should wait for the response.
  * @note	Takes control of the CS line. Setting it low.
  * @retval None
  */
static void spi_notifyBusy(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	__HAL_GPIO_EXTI_CLEAR_IT(SPI_CS_Pin);
	__HAL_GPIO_EXTI_CLEAR_FLAG(SPI_CS_Pin);

	/* EXTI interrupt init*/
	HAL_NVIC_DisableIRQ(EXTI4_15_IRQn);

	/*Configure GPIO pin : SPI_CS_Pin */
	GPIO_InitStruct.Pin = SPI_CS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(SPI_CS_GPIO_Port, &GPIO_InitStruct);

	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_RESET);
}

/**
  * @brief  Notify the master that a response is ready to be requested.
  * @note	Set's the CS line High again, then relinquishes control of
  * 		the CS line.
  * 		again.
  * @retval None
  */
static void spi_notifyReady(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	__HAL_GPIO_EXTI_CLEAR_IT(SPI_CS_Pin);
	__HAL_GPIO_EXTI_CLEAR_FLAG(SPI_CS_Pin);

	HAL_Delay(10);
	HAL_GPIO_WritePin(SPI_CS_GPIO_Port, SPI_CS_Pin, GPIO_PIN_SET);

	/*Configure GPIO pin : SPI_CS_Pin */
	GPIO_InitStruct.Pin = SPI_CS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(SPI_CS_GPIO_Port, &GPIO_InitStruct);

	__HAL_GPIO_EXTI_CLEAR_IT(SPI_CS_Pin);
	__HAL_GPIO_EXTI_CLEAR_FLAG(SPI_CS_Pin);

	/* EXTI interrupt init*/
	HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);

}

/**
  * @brief  Handles a message that has been sent over SPI
  * @note	Gathers the length from the header and then
  * 		prepares to receive the additional data.
  * 		Lastly notifies the MASTER that the message
  * 		has been processed.
  * @retval None
  */
static void spi_handleIncomingHeader(void)
{
	spi_header_t header;
	uint16_t responseLength;

	spi_notifyBusy();

	header.type = spi_rxBuffer[0];

	header.address = spi_rxBuffer[1] << 24;
	header.address |= spi_rxBuffer[2] << 16;
	header.address |= spi_rxBuffer[3] << 8;
	header.address |= spi_rxBuffer[4];

	header.length = spi_rxBuffer[5] << 8;
	header.length |= spi_rxBuffer[6];

	responseLength = spi_handleIncomingData(&header, spi_txBuffer);

	spi_notifyReady();

	utils_awaitFlags(50, 1, flagSpiCsFalling);

	spi_prepareToTxRx(responseLength, spi_txBuffer, spi_rxBuffer);

	utils_awaitFlags(50, 2, flagSpiCsRising, flagSpiTxRxDone);
	if(utils_checkFlag(flagSpiTxRxDone))
	{
		utils_awaitFlags(50, 1, flagSpiCsRising);
	}

	if(header.type == 0x02)
	{
		flash_checkBootState();
	}
}

/**
  * @brief  Handles a message header and requests more data if
  * 		neccessary.
  * @param[in]  header pointer to spi_header_t that is has been processed.
  * @param[out] response pointer to uint8_t data that will contain
  * 		the response to the incoming message.
  *
  * @retval responseLength uint16_t length of the response to be sent.
  */
static uint16_t spi_handleIncomingData(spi_header_t* header, uint8_t* response)
{
	uint32_t crc, crcCalc;

	spi_assert((header->length >= SPI_CRC_SIZE), spiError_length);
	spi_assert((header->length <= (SPI_MAX_DATA_SIZE + SPI_CRC_SIZE)), spiError_length);

	spi_prepareToTxRx(header->length, spi_txBuffer + SPI_HEADER_SIZE, spi_rxBuffer + SPI_HEADER_SIZE);

	utils_awaitFlags(1000, 1, flagSpiTxRxDone);
	spi_assert((utils_checkFlag(flagSpiTxRxDone)), spiError_dataNotReceived);

	header->data = spi_rxBuffer + SPI_HEADER_SIZE;

	crc = header->data[header->length - 4] << 24;
	crc |= header->data[header->length - 3] << 16;
	crc |= header->data[header->length - 2] << 8;
	crc |= header->data[header->length - 1];

	crcCalc = spi_calcCrc(spi_rxBuffer, SPI_HEADER_SIZE + header->length - SPI_CRC_SIZE);

	spi_assert((crc == crcCalc || crc == crcCalc - 1), spiError_crcMismatch);

	header->length -= SPI_CRC_SIZE;

	HAL_IWDG_Refresh(spi_iwdg);

	return flash_handleMessage(header, spi_txBuffer);
}

/**
  * @brief  Calculates the 32bit CRC of the message.
  * @param[in]  data pointer to uint8_t over which the CRC is to be calculated.
  * @param[in]  length uint16_t length of the data.
  * @retval CRC uint32_t calculated CRC.
  */
static uint32_t spi_calcCrc(uint8_t* data, uint16_t length)
{
	return HAL_CRC_Calculate(spi_crc, (uint32_t*)data, (uint32_t)length) ^ 0xFFFFFFFF;
}
