/*
 * spi.h
 *
 *  Created on: Jan 18, 2021
 *      Author: timmy
 */

#ifndef INC_SPI_H_
#define INC_SPI_H_

#include "main.h"

#define SPI_MIN_RESPONSE_SIZE	 3

typedef enum
{
	spiError_noError = 0xA0,
	spiError_length = 0x11,
	spiError_dataNotReceived = 0x12,
	spiError_crcMismatch = 0x13
}spi_error_e;

typedef struct
{
	uint8_t type;
	uint32_t address;
	uint16_t length;
	uint8_t* data;
}spi_header_t;

void spi_init(SPI_HandleTypeDef*, CRC_HandleTypeDef*, IWDG_HandleTypeDef*);
void spi_checkIncoming(void);

#endif /* INC_SPI_H_ */
