/*
 * flash.h
 *
 *  Created on: Jan 18, 2021
 *      Author: timmy
 */

#ifndef INC_FLASH_H_
#define INC_FLASH_H_

#include "main.h"
#include "spi.h"

typedef enum
{
	flashError_noError = 0xA0,
	flashError_invalidType = 0x21,
	flashError_versionOlder = 0x22,
	flashError_addressRange = 0x23,
	flashError_invalidLength = 0x24,
	flashError_addressOffset = 0x25,
	flashError_lockingFailed = 0x26,
	flashError_eraseFailed = 0x27,
	flashError_writeFailed = 0x28,
	flashError_crcMismatch = 0x29
}flash_error_e;

typedef struct
{
	uint32_t crc;
	uint32_t version;
	uint32_t length;

}flash_config_t;

void flash_init(CRC_HandleTypeDef*, TIM_HandleTypeDef*);
void flash_checkBootState(void);
uint16_t flash_handleMessage(spi_header_t*, uint8_t*);
void flash_checkReset(void);


#endif /* INC_FLASH_H_ */
