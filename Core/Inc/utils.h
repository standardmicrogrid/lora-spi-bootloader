/*
 * utils.h
 *
 *  Created on: 18 Jan 2021
 *      Author: timmy
 */

#ifndef INC_UTILS_H_
#define INC_UTILS_H_

#include "main.h"
#include "stdarg.h"

typedef enum
{
	flagSpiCsFalling = 0,
	flagSpiCsRising ,
	flagSpiTxRxDone,
}utils_flag_e;

void utils_setFlag(utils_flag_e thisFlag);
void utils_clearFlag(utils_flag_e thisFlag);
bool utils_checkFlag(utils_flag_e thisFlag);

void utils_awaitFlags(uint32_t msTimeout, int num, ...);

#endif /* INC_UTILS_H_ */
